import CMS from 'netlify-cms'

import '../components/layout.css'

import IntroPreview from './previews/intro'
import AboutPreview from './previews/about'
import WorkPreview from './previews/work'
import ClientPreview from './previews/client'
import ContactPreview from './previews/contact'

CMS.registerPreviewTemplate('intro', IntroPreview)
CMS.registerPreviewTemplate('about', AboutPreview)
CMS.registerPreviewTemplate('works', WorkPreview)
CMS.registerPreviewTemplate('clients', ClientPreview)
CMS.registerPreviewTemplate('contact', ContactPreview)
