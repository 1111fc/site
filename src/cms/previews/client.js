import React from 'react'
import { StyleSheetManager } from 'styled-components'

import ClientTemplate from '../../templates/client'

const ClientPreview = ({ entry }) => {
  const iframe = document.querySelector('iframe[class^="css-"]')
  const iframeHeadElem = iframe.contentDocument.head

  return (
    <StyleSheetManager target={iframeHeadElem}>
      <div style={{ maxWidth: '12rem', padding: '2rem' }}>
        <ClientTemplate
          src={entry.getIn(['data', 'image'])}
          title={entry.getIn(['data', 'title'])}
          showTitle={entry.getIn(['data', 'showTitle'])}
          animation={false}
        />
      </div>
    </StyleSheetManager>
  )
}

export default ClientPreview
