import React from 'react'
import { StyleSheetManager } from 'styled-components'

import IntroTemplate from '../../templates/intro'

const IntroPreview = ({ entry }) => {
  const iframe = document.querySelector('iframe[class^="css-"]')
  const iframeHeadElem = iframe.contentDocument.head

  return (
    <StyleSheetManager target={iframeHeadElem}>
      <div style={{ padding: '2rem' }}>
        <div style={{ position: 'relative', padding: '2rem', height: '24rem' }}>
          <IntroTemplate
            videoId={entry.getIn(['data', 'videoId'])}
            image={entry.getIn(['data', 'image'])}
            title={entry.getIn(['data', 'title'])}
          />
        </div>
      </div>
    </StyleSheetManager>
  )
}

export default IntroPreview
