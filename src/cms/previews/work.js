import React from 'react'
import { StyleSheetManager } from 'styled-components'

import WorkTemplate from '../../templates/work'

const WorkPreview = ({ entry, widgetFor }) => {
  const iframe = document.querySelector('iframe[class^="css-"]')
  const iframeHeadElem = iframe.contentDocument.head

  return (
    <StyleSheetManager target={iframeHeadElem}>
      <div style={{ padding: '2rem' }}>
        <WorkTemplate
          videoId={entry.getIn(['data', 'videoId'])}
          title={entry.getIn(['data', 'title'])}
          client={entry.getIn(['data', 'client'])}
          body={entry.getIn(['data', 'body'])}
          animation={false}
        />
      </div>
    </StyleSheetManager>
  )
}

export default WorkPreview
