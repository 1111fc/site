import React from 'react'
import { StyleSheetManager } from 'styled-components'

import AboutTemplate from '../../templates/about'

const AboutPreview = ({ entry }) => {
  const iframe = document.querySelector('iframe[class^="css-"]')
  const iframeHeadElem = iframe.contentDocument.head

  return (
    <StyleSheetManager target={iframeHeadElem}>
      <div style={{ padding: '2rem' }}>
        <AboutTemplate
          title={entry.getIn(['data', 'title'])}
          body={entry.getIn(['data', 'body'])}
          animation={false}
        />
      </div>
    </StyleSheetManager>
  )
}

export default AboutPreview
