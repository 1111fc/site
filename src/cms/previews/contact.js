import React from 'react'
import { StyleSheetManager } from 'styled-components'

import ContactTemplate from '../../templates/contact'

const ContactPreview = ({ entry, widgetFor, widgetsFor }) => {
  const social = entry.getIn(['data', 'social']).map(item => ({
    text: item.getIn(['text']),
    link: item.getIn(['link']),
  }))

  const iframe = document.querySelector('iframe[class^="css-"]')
  const iframeHeadElem = iframe.contentDocument.head

  return (
    <StyleSheetManager target={iframeHeadElem}>
      <div style={{ padding: '2rem' }}>
        <ContactTemplate
          title={entry.getIn(['data', 'title'])}
          body={entry.getIn(['data', 'body'])}
          social={social}
          animation={false}
        />
      </div>
    </StyleSheetManager>
  )
}

export default ContactPreview
