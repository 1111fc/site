import React from 'react'

import Layout from '../components/layout'

import SectionIntro from '../sections/intro'
import SectionAbout from '../sections/about'
import SectionWorks from '../sections/works'
import SectionClients from '../sections/clients'
import SectionContact from '../sections/contact'

const IndexPage = () => (
  <Layout>
    <SectionIntro />
    <SectionAbout />
    <SectionWorks />
    <SectionClients />
    <SectionContact />

    <svg
      width="0"
      height="0"
      style={{
        position: 'fixed',
        top: 0,
        left: 0,
      }}
    >
      <filter id="blur">
        <feGaussianBlur id="blur-value" stdDeviation="6" />
      </filter>
    </svg>
  </Layout>
)

export default IndexPage
