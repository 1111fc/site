import React from 'react'
import Layout from '../components/layout'

import { Consumer as SiteConsumer } from '../components/context'

import SectionStyle from '../styles/section'
import ErrorStyle from '../styles/errors'

const NotFoundPage = () => (
  <Layout>
    <SiteConsumer>
      {({ state }) => {
        const { showMenu } = state

        return (
          <SectionStyle.Container
            fullHeight
            blurred={!!showMenu}
            id="trabalhos"
          >
            <SectionStyle.Content fullHeight flex jCenter aCenter>
              <ErrorStyle.Container>
                <h1>404</h1>
                <p>Página não encontrada</p>
              </ErrorStyle.Container>
            </SectionStyle.Content>
          </SectionStyle.Container>
        )
      }}
    </SiteConsumer>
  </Layout>
)

export default NotFoundPage
