import React from 'react'
import ScrollAnimation from 'react-animate-on-scroll'
import Container from '../styles/container'
import { List } from '../styles/list'

const TemplateTitle = ({ title }) => <h2>{title}</h2>

const TemplateBody = ({ body, social }) => (
  <>
    <div dangerouslySetInnerHTML={{ __html: body }} />

    <List inline>
      {social.map((item, index) => (
        <li key={`${index}-${item.link}`}>
          <a href={item.link}>
            <strong>
              <small>{item.text}</small>
            </strong>
          </a>
        </li>
      ))}
    </List>
  </>
)

const ContactTemplate = ({ title, body, social = [], animation = true }) => {
  return (
    <Container>
      {animation ? (
        <ScrollAnimation animateIn="fadeIn" duration={0.5} animateOnce={true}>
          <ScrollAnimation
            animateIn="slideInUp"
            duration={0.5}
            animateOnce={true}
          >
            <TemplateTitle title={title} />
          </ScrollAnimation>
        </ScrollAnimation>
      ) : (
        <TemplateTitle title={title} />
      )}

      {animation ? (
        <ScrollAnimation
          animateIn="fadeIn"
          duration={0.5}
          delay={500}
          animateOnce={true}
        >
          <ScrollAnimation
            animateIn="slideInUp"
            duration={0.5}
            delay={500}
            animateOnce={true}
          >
            <TemplateBody body={body} social={social} />
          </ScrollAnimation>
        </ScrollAnimation>
      ) : (
        <TemplateBody body={body} social={social} />
      )}
    </Container>
  )
}

export default ContactTemplate
