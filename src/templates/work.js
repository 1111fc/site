import React from 'react'
import ScrollAnimation from 'react-animate-on-scroll'
import Video from '../components/video'
import WorkStyle from '../styles/works'

class Template extends React.Component {
  state = {
    showInfo: false,
  }

  render() {
    const {
      videoId,
      thumbnail,
      title,
      client,
      onSelect = () => {},
    } = this.props

    return (
      <ScrollAnimation animateIn="fadeIn" duration={0.5} animateOnce={true}>
        <WorkStyle.Item
          onMouseEnter={() => this.setState({ showInfo: true })}
          onMouseLeave={() => this.setState({ showInfo: false })}
          onClick={() => onSelect(videoId)}
        >
          {videoId && (
            <Video
              videoId={videoId}
              thumbnail={thumbnail}
              onSelect={onSelect}
            />
          )}

          <WorkStyle.Overlay show={this.state.showInfo}>
            <div>
              {title && <WorkStyle.Title>{title}</WorkStyle.Title>}

              {client && <WorkStyle.Client>{client}</WorkStyle.Client>}
            </div>
          </WorkStyle.Overlay>
        </WorkStyle.Item>
      </ScrollAnimation>
    )
  }
}

const WorkTemplate = ({ animation = true, delay = 0, ...props }) =>
  animation ? (
    <ScrollAnimation
      animateIn="fadeIn"
      duration={0.5}
      animateOnce={true}
      delay={delay}
    >
      <ScrollAnimation
        animateIn="slideInUp"
        duration={0.5}
        animateOnce={true}
        delay={delay}
      >
        <Template {...props} />
      </ScrollAnimation>
    </ScrollAnimation>
  ) : (
    <Template {...props} />
  )

export default WorkTemplate
