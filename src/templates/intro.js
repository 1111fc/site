import React, { Component } from 'react'
import Play from '../components/play'
import IntroStyle from '../styles/intro'

class IntroTemplate extends Component {
  state = {
    showPlayer: false,
    hasImageLoaded: false,
  }

  handleShowPlayer = showPlayer => {
    this.setState({ showPlayer }, () => {
      if (showPlayer) {
        document.body.style.overflow = 'hidden'
      } else {
        document.body.style.overflow = 'visible'
      }
    })
  }

  render() {
    const { showPlayer } = this.state
    const { videoId, image, title } = this.props

    return (
      <>
        {showPlayer && (
          <IntroStyle.Wrapper>
            <IntroStyle.Backdrop onClick={() => this.handleShowPlayer(false)} />

            <IntroStyle.Close onClick={() => this.handleShowPlayer(false)}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
              >
                <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
                <path d="M0 0h24v24H0z" fill="none" />
              </svg>
            </IntroStyle.Close>

            <IntroStyle.Video
              type="text/html"
              src={`https://www.youtube.com/embed/${videoId}?autoplay=1&rel=0&showInfo=0`}
              frameBorder="0"
            />
          </IntroStyle.Wrapper>
        )}

        <IntroStyle.Image>
          <img src={image} alt="" />
        </IntroStyle.Image>

        <IntroStyle.Title>
          <button onClick={() => this.handleShowPlayer(true)}>
            <Play />
          </button>

          <div>{title}</div>
        </IntroStyle.Title>
      </>
    )
  }
}

export default IntroTemplate
