import React from 'react'
import ScrollAnimation from 'react-animate-on-scroll'
import Style from '../styles/clients'

const Template = ({ title, src, showTitle }) => (
  <Style.Container>
    <Style.Image>{src && <img src={src} alt="" />}</Style.Image>

    {showTitle && (
      <Style.Title>
        <small>{title}</small>
      </Style.Title>
    )}
  </Style.Container>
)

const ClientTemplate = ({
  title,
  src,
  showTitle,
  delay = 0,
  animation = true,
}) => (
  <ScrollAnimation
    animateIn="fadeIn"
    duration={0.5}
    animateOnce={true}
    delay={delay}
  >
    <ScrollAnimation
      animateIn="slideInUp"
      duration={0.5}
      animateOnce={true}
      delay={delay}
    >
      <Template title={title} src={src} showTitle={showTitle} />
    </ScrollAnimation>
  </ScrollAnimation>
)

export default ClientTemplate
