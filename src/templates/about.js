import React from 'react'
import ScrollAnimation from 'react-animate-on-scroll'
import LoadedVideo from '../components/loadedVideo'
import Container from '../styles/container'

const TemplateVideo = ({ videoId }) => <LoadedVideo videoId={videoId} />

const TemplateTitle = ({ title }) => <h2>{title}</h2>

const TemplateBody = ({ body }) => (
  <div dangerouslySetInnerHTML={{ __html: body }} />
)

const AboutTemplate = ({ videoId, title, body, animation = true }) => {
  return (
    <div style={{ width: '100%' }}>
      {animation ? (
        <ScrollAnimation animateIn="fadeIn" duration={0.5} animateOnce={true}>
          <ScrollAnimation
            animateIn="slideInUp"
            duration={0.5}
            animateOnce={true}
          >
            <TemplateVideo videoId={videoId} />
          </ScrollAnimation>
        </ScrollAnimation>
      ) : (
        <TemplateVideo videoId={videoId} />
      )}

      <div style={{ marginTop: '4rem' }}>
        <Container>
          {animation ? (
            <ScrollAnimation
              animateIn="fadeIn"
              duration={0.5}
              animateOnce={true}
            >
              <ScrollAnimation
                animateIn="slideInUp"
                duration={0.5}
                animateOnce={true}
              >
                <TemplateTitle title={title} />
              </ScrollAnimation>
            </ScrollAnimation>
          ) : (
            <TemplateTitle title={title} />
          )}

          {animation ? (
            <ScrollAnimation
              animateIn="fadeIn"
              duration={0.5}
              delay={500}
              animateOnce={true}
            >
              <ScrollAnimation
                animateIn="slideInUp"
                duration={0.5}
                delay={500}
                animateOnce={true}
              >
                <TemplateBody body={body} />
              </ScrollAnimation>
            </ScrollAnimation>
          ) : (
            <TemplateBody body={body} />
          )}
        </Container>
      </div>
    </div>
  )
}

export default AboutTemplate
