import { graphql, StaticQuery } from 'gatsby'
import React, { Component } from 'react'
import { Consumer as SiteConsumer } from '../components/context'
import ClientStyle from '../styles/clients'
import SectionStyle from '../styles/section'
import ClientTemplate from '../templates/client'

const collator = new Intl.Collator('pt-br')

class SectionClients extends Component {
  render() {
    return (
      <StaticQuery
        query={graphql`
          query SectionClientsQuery {
            clients: allMarkdownRemark(
              filter: { fileAbsolutePath: { regex: "/clients/" } }
            ) {
              edges {
                node {
                  fileAbsolutePath
                  frontmatter {
                    order
                    image
                    title
                  }
                }
              }
            }
          }
        `}
        render={({ clients }) => {
          return (
            <SiteConsumer>
              {({ state, actions }) => {
                const { showMenu } = state

                return (
                  <SectionStyle.Container blurred={!!showMenu} id="clientes">
                    <SectionStyle.Content flex jCenter aCenter>
                      <ClientStyle.Wrapper>
                        {clients.edges
                          .sort(
                            (a, b) =>
                              Number(b.node.frontmatter.order) -
                              Number(a.node.frontmatter.order) +
                              collator.compare(
                                b.node.fileAbsolutePath,
                                a.node.fileAbsolutePath
                              )
                          )
                          .map(({ node: { frontmatter: client } }, index) => (
                            <ClientTemplate
                              key={`client-${index}`}
                              src={client.image}
                              title={client.title}
                              showTitle={client.showTitle}
                              delay={index * 125}
                            />
                          ))}
                      </ClientStyle.Wrapper>
                    </SectionStyle.Content>
                  </SectionStyle.Container>
                )
              }}
            </SiteConsumer>
          )
        }}
      />
    )
  }
}

export default SectionClients
