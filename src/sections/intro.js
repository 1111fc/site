import { graphql, StaticQuery } from 'gatsby'
import React, { Component } from 'react'
import { Consumer as SiteConsumer } from '../components/context'
import SectionStyle from '../styles/section'
import IntroTemplate from '../templates/intro'

const collator = new Intl.Collator('pt-br')

class SectionIntro extends Component {
  state = {
    currentIndex: 0,
    slideshow: true,
  }

  slideshowInterval = null

  componentDidMount() {
    this.setupSlideshow()
  }

  componentWillUnmount() {
    this.teardownSlideshow()
  }

  setupSlideshow = () => {
    if (!this.slideshowInterval) {
      const { featured } = this.props
      this.slideshowInterval = setInterval(() => {
        this.setState(state => ({
          currentIndex:
            state.currentIndex + 1 >= featured.edges.length
              ? 0
              : state.currentIndex + 1,
        }))
      }, 10000)
    }
  }

  teardownSlideshow = () => {
    if (this.slideshowInterval) {
      clearInterval(this.slideshowInterval)

      this.slideshowInterval = null
    }
  }

  render() {
    const { featured } = this.props

    return (
      <SiteConsumer>
        {({ state, actions }) => {
          const { showMenu } = state

          return (
            <SectionStyle.Container
              fullHeight="always"
              blurred={!!showMenu}
              id="inicio"
            >
              <SectionStyle.Content>
                {featured.edges
                  .sort(
                    (a, b) =>
                      Number(b.node.frontmatter.order) -
                      Number(a.node.frontmatter.order) +
                      collator.compare(
                        b.node.fileAbsolutePath,
                        a.node.fileAbsolutePath
                      )
                  )
                  .map(
                    ({ node: { frontmatter } }, index) =>
                      index === this.state.currentIndex && (
                        <IntroTemplate
                          key={frontmatter.videoId}
                          videoId={frontmatter.videoId}
                          image={frontmatter.image}
                          title={frontmatter.title}
                          onStatusChange={status => {
                            if (status === 'ended') {
                              this.setupSlideshow()
                            } else if (
                              status === 'playing' ||
                              status === 'paused'
                            ) {
                              this.teardownSlideshow()
                            }
                          }}
                        />
                      )
                  )}
              </SectionStyle.Content>
            </SectionStyle.Container>
          )
        }}
      </SiteConsumer>
    )
  }
}

const WrapperSectionIntro = props => (
  <StaticQuery
    query={graphql`
      query SectionIntroQuery {
        featured: allMarkdownRemark(
          filter: { fileAbsolutePath: { regex: "/featured/" } }
        ) {
          edges {
            node {
              fileAbsolutePath
              frontmatter {
                order
                videoId
                image
                title
              }
            }
          }
        }
      }
    `}
    render={({ featured }) => {
      return <SectionIntro {...props} featured={featured} />
    }}
  />
)

export default WrapperSectionIntro
