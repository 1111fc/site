import { graphql, StaticQuery } from 'gatsby'
import React from 'react'
import { Consumer as SiteConsumer } from '../components/context'
import SectionStyle from '../styles/section'
import ContactTemplate from '../templates/contact'

const SectionContact = () => {
  return (
    <StaticQuery
      query={graphql`
        query SectionContactQuery {
          contact: markdownRemark(fileAbsolutePath: { regex: "/contact/" }) {
            frontmatter {
              title
              social {
                link
                text
              }
            }
            html
          }
        }
      `}
      render={({ contact }) => {
        return (
          <SiteConsumer>
            {({ state, actions }) => {
              const { showMenu } = state

              return (
                <SectionStyle.Container
                  fullHeight
                  blurred={!!showMenu}
                  id="contato"
                >
                  <SectionStyle.Content fullHeight flex jCenter aCenter>
                    <ContactTemplate
                      title={contact.frontmatter.title}
                      body={contact.html}
                      social={contact.frontmatter.social}
                    />
                  </SectionStyle.Content>
                </SectionStyle.Container>
              )
            }}
          </SiteConsumer>
        )
      }}
    />
  )
}

export default SectionContact
