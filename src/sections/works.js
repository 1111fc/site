import { graphql, StaticQuery } from 'gatsby'
import React, { Component } from 'react'
import { Consumer as SiteConsumer } from '../components/context'
import SectionStyle from '../styles/section'
import WorkStyle from '../styles/works'
import WorkTemplate from '../templates/work'

const collator = new Intl.Collator('pt-br')

class ModalInfo extends Component {
  state = {
    shown: false,
  }

  render() {
    return (
      <WorkStyle.ModalInfo shown={this.state.shown}>
        <WorkStyle.ModalInfoToggle
          onClick={() => this.setState(state => ({ shown: !state.shown }))}
        >
          <h5>CRÉDITOS</h5>
          {this.state.shown ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
            >
              <path d="M7 10l5 5 5-5z" />
              <path d="M0 0h24v24H0z" fill="none" />
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
            >
              <path d="M7 14l5-5 5 5z" />
              <path d="M0 0h24v24H0z" fill="none" />
            </svg>
          )}
        </WorkStyle.ModalInfoToggle>

        {this.state.shown && (
          <WorkStyle.ModalInfoContent
            shown={this.state.shown}
            dangerouslySetInnerHTML={{
              __html: this.props.html,
            }}
          />
        )}
      </WorkStyle.ModalInfo>
    )
  }
}

const getWorkByVideoId = (works, videoId) =>
  works.edges.find(
    ({ node: { frontmatter: work } }) => work.videoId === videoId
  )

class SectionWorks extends Component {
  state = {
    selectedVideoId: null,
  }

  render() {
    const { works } = this.props
    const selectedWork = getWorkByVideoId(works, this.state.selectedVideoId)

    return (
      <SiteConsumer>
        {({ state }) => {
          const { showMenu } = state

          return (
            <SectionStyle.Container blurred={!!showMenu} id="trabalhos">
              <SectionStyle.Content>
                <WorkStyle.Works>
                  <WorkStyle.List>
                    {works.edges
                      .sort(
                        (a, b) =>
                          Number(b.node.frontmatter.order) -
                          Number(a.node.frontmatter.order) +
                          collator.compare(
                            b.node.fileAbsolutePath,
                            a.node.fileAbsolutePath
                          )
                      )
                      .map(({ node: { frontmatter: work, html } }, index) => {
                        return (
                          <WorkTemplate
                            key={`${index}-${work.videoId}-${work.title}`}
                            videoId={work.videoId}
                            thumbnail={work.thumbnail}
                            title={work.title}
                            client={work.client}
                            body={html}
                            delay={index * 125}
                            onSelect={videoId =>
                              this.setState({ selectedVideoId: videoId })
                            }
                          />
                        )
                      })}
                  </WorkStyle.List>
                </WorkStyle.Works>

                {this.state.selectedVideoId && (
                  <WorkStyle.Modal>
                    <WorkStyle.ModalBackdrop
                      onClick={() => this.setState({ selectedVideoId: null })}
                    />

                    <WorkStyle.ModalClose
                      onClick={() => this.setState({ selectedVideoId: null })}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                      >
                        <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
                        <path d="M0 0h24v24H0z" fill="none" />
                      </svg>
                    </WorkStyle.ModalClose>

                    <WorkStyle.ModalBody>
                      <iframe
                        id="player"
                        type="text/html"
                        src={`https://www.youtube.com/embed/${
                          this.state.selectedVideoId
                        }`}
                        frameborder="0"
                      />

                      <ModalInfo html={selectedWork.node.html} />
                    </WorkStyle.ModalBody>
                  </WorkStyle.Modal>
                )}
              </SectionStyle.Content>
            </SectionStyle.Container>
          )
        }}
      </SiteConsumer>
    )
  }
}

const WrappedSectionWorks = props => (
  <StaticQuery
    query={graphql`
      query SectionWorksQuery {
        works: allMarkdownRemark(
          filter: { fileAbsolutePath: { regex: "/works/" } }
        ) {
          edges {
            node {
              fileAbsolutePath
              frontmatter {
                order
                videoId
                thumbnail
                title
                client
              }
              html
            }
          }
        }
      }
    `}
    render={({ works }) => {
      return <SectionWorks {...props} works={works} />
    }}
  />
)

export default WrappedSectionWorks
