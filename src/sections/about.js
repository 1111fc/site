import { graphql, StaticQuery } from 'gatsby'
import React from 'react'
import { Consumer as SiteConsumer } from '../components/context'
import SectionStyle from '../styles/section'
import AboutTemplate from '../templates/about'

const SectionAbout = () => {
  return (
    <StaticQuery
      query={graphql`
        query SectionAboutQuery {
          about: markdownRemark(fileAbsolutePath: { regex: "/about/" }) {
            frontmatter {
              title
              videoId
            }
            html
          }
        }
      `}
      render={({ about }) => {
        return (
          <SiteConsumer>
            {({ state, actions }) => {
              const { showMenu } = state

              return (
                <SectionStyle.Container
                  fullHeight
                  blurred={!!showMenu}
                  id="quem-somos"
                >
                  <SectionStyle.Content fullHeight flex jCenter aCenter>
                    <AboutTemplate
                      title={about.frontmatter.title}
                      videoId={about.frontmatter.videoId}
                      body={about.html}
                    />
                  </SectionStyle.Content>
                </SectionStyle.Container>
              )
            }}
          </SiteConsumer>
        )
      }}
    />
  )
}

export default SectionAbout
