import React, { Component } from 'react'

export const { Provider, Consumer } = React.createContext('site')

class SiteContext extends Component {
  state = {
    hasMenuBeenTouched: false,
    showMenu: false,
    observer: null,
  }

  actions = {
    TOGGLE_MENU: state =>
      this.setState({
        showMenu: state,
        hasMenuBeenTouched: true,
      }),

    OBSERVE: elem => {
      this.state.observer.observe(elem)
    },
  }

  observerCallback = (entries, observer) => {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        entry.target.classList.add('animate')
      }
    })
  }

  componentDidMount() {
    if ('IntersectionObserver' in window) {
      const options = {
        rootMargin: '0px',
        threshold: 1.0,
      }
      const observer = new window.IntersectionObserver(
        this.observerCallback,
        options
      )

      this.setState({ observer })
    }
  }

  render() {
    return (
      <Provider value={{ state: this.state, actions: this.actions }}>
        {this.props.children}
      </Provider>
    )
  }
}

export default SiteContext
