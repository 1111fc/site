import React from 'react'

import MenuStyle from '../styles/menu'

const links = [
  { url: '/#inicio', text: 'INTRO' },
  { url: '/#quem-somos', text: 'QUEM SOMOS' },
  { url: '/#trabalhos', text: 'TRABALHOS' },
  { url: '/#clientes', text: 'CLIENTES' },
  { url: '/#contato', text: 'CONTATO' },
]

const Menu = ({ isMobile, status, show, toggle }) => (
  <MenuStyle.Container isMobile={isMobile} active={show}>
    {links.map((link, index) => (
      <MenuStyle.Item
        key={`${index}-${link.url}-${link.text}`}
        isMobile={isMobile}
        status={status}
        style={{
          animationDelay: show
            ? `${(index + 1) * 50}ms`
            : `${(links.length - 1 - index) * 50}ms`,
        }}
      >
        <a href={link.url} onClick={() => toggle(false)}>
          {link.text}
        </a>
      </MenuStyle.Item>
    ))}
  </MenuStyle.Container>
)

export default Menu
