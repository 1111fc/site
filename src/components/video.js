import React from 'react'

import VideoStyle from '../styles/video'

const Video = ({ thumbnail, videoId }) => {
  const thumbnailSrc =
    thumbnail || `https://img.youtube.com/vi/${videoId}/0.jpg`

  return (
    <VideoStyle.Wrapper>
      <VideoStyle.Thumbnail src={thumbnailSrc} />
    </VideoStyle.Wrapper>
  )
}

export default Video
