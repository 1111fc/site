import React, { Component } from 'react'

import LoadedVideoStyle from '../styles/loadedVideo'

import YTPlayer from 'yt-player'

class LoadedVideo extends Component {
  state = {
    player: null,
  }

  container = React.createRef()

  componentDidMount() {
    const player = new YTPlayer(this.container.current, {
      controls: false,
      annotations: false,
      info: false,
      related: false,
      modestbranding: true,
      showinfo: false,
      disablekb: true,
    })

    this.setState({ player }, () => player.load(this.props.videoId))
  }

  render() {
    return (
      <LoadedVideoStyle.Wrapper>
        <LoadedVideoStyle.Container ref={this.container} />
      </LoadedVideoStyle.Wrapper>
    )
  }
}

export default LoadedVideo
