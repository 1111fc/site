import React from 'react'

import BurgerStyle from '../styles/burger'

const Burger = ({ show, toggle }) => (
  <BurgerStyle.Container onClick={() => toggle(!show)}>
    <BurgerStyle.Label active={show}>MENU</BurgerStyle.Label>

    <BurgerStyle.Icon active={show}>
      <span />
      <span />
      <span />
    </BurgerStyle.Icon>
  </BurgerStyle.Container>
)

export default Burger
