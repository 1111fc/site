import React, { Component } from 'react'
import componentQueries from 'react-component-queries'

import * as HeaderStyle from '../styles/header'

import Logo from './logo'
import Burger from './burger'
import Menu from './menu'

import { Consumer as SiteConsumer } from './context'

class Header extends Component {
  componentDidUpdate(prevProps) {
    if (prevProps.isMobile !== this.props.isMobile && !this.props.isMobile) {
      this.props.actions.TOGGLE_MENU()
    }
  }

  render() {
    const { hasMenuBeenTouched, showMenu } = this.props.state
    const { TOGGLE_MENU } = this.props.actions

    return (
      <HeaderStyle.Container isMobile={this.props.isMobile} overlay={showMenu}>
        <HeaderStyle.Nav isMobile={this.props.isMobile}>
          <HeaderStyle.Logo>
            <a
              href="/#inicio"
              onClick={() => (showMenu ? TOGGLE_MENU(false) : null)}
            >
              <Logo />
            </a>
          </HeaderStyle.Logo>

          <HeaderStyle.Menu>
            {this.props.isMobile && (
              <Burger show={showMenu} toggle={TOGGLE_MENU} />
            )}
            <Menu
              isMobile={this.props.isMobile}
              status={
                !this.props.isMobile || showMenu
                  ? 'active'
                  : hasMenuBeenTouched
                  ? 'inactive'
                  : ''
              }
              show={showMenu}
              toggle={TOGGLE_MENU}
            />
          </HeaderStyle.Menu>
        </HeaderStyle.Nav>
      </HeaderStyle.Container>
    )
  }
}

const HeaderWithConsumer = props => (
  <SiteConsumer>
    {({ state, actions }) => {
      return <Header actions={actions} state={state} {...props} />
    }}
  </SiteConsumer>
)

export default componentQueries(({ width }) => {
  return {
    isMobile: width < 768,
  }
})(HeaderWithConsumer)
