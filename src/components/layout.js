import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

import 'animate.css/animate.min.css'
import './layout.css'

import Header from './header'

import SiteContext from './context'

const Layout = ({ children, data }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet title={data.site.siteMetadata.title}>
          <html lang="pt-br" />
          <script src="https://polyfill.io/v2/polyfill.min.js?features=IntersectionObserver" />
          <style jsx global>{`
            @keyframes slideInUp {
              from {
                transform: translate(0, 1rem);
              }
              to {
                transform: translate(0, 0);
              }
            }
          `}</style>
        </Helmet>

        <SiteContext>
          <Header siteTitle={data.site.siteMetadata.title} />

          <div>{children}</div>
        </SiteContext>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
