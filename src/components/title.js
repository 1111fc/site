import React, { Component } from 'react'

import TitleStyle from '../styles/title'

class Title extends Component {
  state = {
    observer: null,
    spans: this.props.children.split('').map((letter, index, arr) => (
      <TitleStyle.Letter
        key={`${letter}-${index}`}
        style={{ '--delay': `${index * 50}ms` }}
      >
        <TitleStyle.Text>{letter}</TitleStyle.Text>
      </TitleStyle.Letter>
    )),
  }

  container = React.createRef()

  handleObserver = (entries, observer) => {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        entry.target.classList.add('animate')
      }
    })
  }

  componentDidMount() {
    const observer = new window.IntersectionObserver(this.handleObserver, {
      rootMargin: '0px',
      threshold: 1.0,
    })

    observer.observe(this.container.current)

    this.setState({ observer })
  }

  render() {
    return (
      <TitleStyle.Container ref={this.container}>
        {this.state.spans}
      </TitleStyle.Container>
    )
  }
}

export default Title
