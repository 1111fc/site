import React from 'react'

const Pause = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="64"
    height="64"
    viewBox="0 0 24 24"
  >
    <path d="M8 7h3v10H8zM13 7h3v10h-3z" />
  </svg>
)

export default Pause
