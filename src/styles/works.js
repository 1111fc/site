import styled from 'styled-components'

const Work = {}

Work.List = styled.ul`
  position: relative;
  z-index: 0;

  list-style: none;
  margin: 0 0.5rem;
  padding: 0 0.5rem;

  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(18rem, 1fr));
  align-content: flex-start;
  width: 100%;
`

Work.Item = styled.li`
  cursor: pointer;
  position: relative;
  width: 100%;
  list-style: none;
`

Work.Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1rem;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.75);
  text-align: center;
  transition: opacity 0.25s ease;

  ${({ show }) =>
    show
      ? `opacity: 1; pointer-events: auto;`
      : `opacity: 0; pointer-events: none;`};
`

Work.Title = styled.h3`
  margin-bottom: 0.5rem;
`

Work.Client = styled.div`
  font-weight: bold;
  color: var(--gray);
`

Work.Description = styled.div`
  font-size: 0.75rem;
  color: var(--gray);

  p {
    margin: 0.25rem 0;
  }
`

Work.Works = styled.div`
  position: relative;
  display: flex;
  width: 100%;
`

Work.Modal = styled.div`
  position: fixed;
  z-index: 15;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 4rem;
  width: 100%;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.75);

  @media screen and (max-width: 600px) {
    padding: 2rem;
  }
`

Work.ModalClose = styled.button`
  position: absolute;
  top: 1rem;
  right: 1rem;
  z-index: 1;
  cursor: pointer;
  background-color: transparent;
  border: none;
  color: white;
  font-size: 2em;
`

Work.ModalBackdrop = styled.div`
  position: fixed;
  z-index: 0;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  background-color: var(--black);
`

Work.ModalBody = styled.div`
  position: relative;
  z-index: 1;
  width: 100%;
  height: 100%;

  iframe {
    display: block;
    width: 100%;
    height: 100%;
  }
`

Work.ModalInfo = styled.div`
  position: absolute;
  left: 0;
  bottom: 0;
  margin-top: 1rem;
  padding: 1rem 1rem 1rem 0;
  max-width: 20rem;
  width: 100%;
  background-color: var(--black);
  font-size: 0.85rem;
  transform: translate(0, 100%);

  @media screen and (max-width: 600px) {
    margin: 1rem 0;
  }

  ${({ shown }) =>
    shown &&
    `
    transform: translate(0, 0);
  `};
`

Work.ModalInfoContent = styled.div``

Work.ModalInfoToggle = styled.button`
  cursor: pointer;
  padding: none;
  border: none;
  background-color: transparent;

  display: flex;
  align-items: center;

  h5 {
    margin: 0;
  }
`

export default Work
