import { css } from 'styled-components'

export const fullHeight = css`
  ${({ fullHeight }) =>
    fullHeight &&
    css`
      min-height: 100vh;

      ${fullHeight !== 'always' &&
        css`
          @media screen and (max-width: 425px) {
            min-height: auto;
          }
        `};
    `};
`

export const flex = css`
  ${({ flex }) =>
    flex &&
    css`
      display: flex;
    `};
`

export const aCenter = css`
  ${({ aCenter }) =>
    aCenter &&
    css`
      align-items: center;
    `};
`

export const jCenter = css`
  ${({ jCenter }) =>
    jCenter &&
    css`
      justify-content: center;
    `};
`
