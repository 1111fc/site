import styled, { keyframes } from 'styled-components'

const blink = keyframes`
  50% {
    background-color: var(--black);
  }
`

const Video = {}

Video.Wrapper = styled.div`
  position: relative;
  flex-grow: 1;
`

Video.Thumbnail = styled.img`
  display: block;
`

Video.Container = styled.div`
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  background-color: var(--darker-gray);
  animation: ${blink} 2s ease both infinite;
`

export default Video
