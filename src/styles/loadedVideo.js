import styled, { keyframes } from 'styled-components'

const blink = keyframes`
  50% {
    background-color: var(--black);
  }
`

const LoadedVideo = {}

LoadedVideo.Wrapper = styled.div`
  position: relative;
  flex-grow: 1;
  margin: 0 -2rem;

  &::before {
    content: '';
    display: block;
    padding-bottom: 56.25%;
  }
`

LoadedVideo.Container = styled.div`
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  background-color: var(--darker-gray);
  animation: ${blink} 2s ease both infinite;

  iframe {
    max-height: 100vh;
  }
`

export default LoadedVideo
