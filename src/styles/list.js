import styled, { css } from 'styled-components'

export const List = styled.ul`
  ${({ inline }) => css`
    margin-top: 2rem;
    padding: 0;

    li {
      display: ${inline ? 'inline-block' : 'block'};

      &:not(:last-child)::after {
        ${inline &&
          css`
            content: '';
            display: inline-block;
            vertical-align: middle;
            margin: 0 0.5rem;
            width: 0.25rem;
            height: 0.25rem;
            background-color: var(--orange, white);
          `};
      }
    }

    a {
      color: inherit;
      text-decoration: inherit;
    }
  `};
`
