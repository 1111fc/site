import styled, { css } from 'styled-components'

import { fullHeight, flex, jCenter, aCenter } from './helpers'

const Section = {}

Section.Container = styled.section`
  ${({ blurred }) => css`
    position: relative;
    width: 100%;
    overflow: hidden;
    box-shadow: 0 0.0625rem 0 0 var(--darker-gray);
    transition: filter 0.25s 0.25s ease;

    ${blurred &&
      css`
        filter: url('#blur');
        filter: blur(10px);
        transition: filter 0.25s ease;
      `};

    @media screen and (max-width: 425px) {
      transition: none !important;
    }

    ${fullHeight};
  `};
`

Section.Content = styled.div`
  padding: calc(4.5rem + var(--padding)) var(--padding);

  @media screen and (max-width: 425px) {
    padding: calc(2.5rem + var(--padding)) var(--padding);
  }

  ${fullHeight};
  ${flex};
  ${jCenter};
  ${aCenter};
`

Section.Title = styled.div`
  position: absolute;
  z-index: 2;
  right: var(--padding);
  top: 6rem;
  writing-mode: vertical-lr;
  text-orientation: sideways;
  color: var(--dark-gray);
  letter-spacing: 0.25em;
  font-size: 0.9em;

  @media screen and (max-width: 425px) {
    top: 3rem;
  }

  h1 {
    margin: 0;
    line-height: 1;
  }
`

export default Section
