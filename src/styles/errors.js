import styled from 'styled-components'

const Err = {}

Err.Container = styled.div`
  position: relative;
  right: 2rem;
  padding: 2rem;
  text-align: center;
  box-shadow: -3px -3px 0 2px var(--black), -4px -4px 0 2px var(--white),
    3px 3px 0 2px var(--black), 4px 4px 0 2px var(--white);
  font-size: calc(0.5vw + 1em);
`

export default Err
