import styled, { keyframes } from 'styled-components'

const show = keyframes`
  from {
    opacity: 0;
    transform: translate(0, 100%)
  }
  to {
    opacity: 1;
    transform: translate(0, 0)
  }
`

const Title = {}

Title.Text = styled.span`
  display: inline-block;
  opacity: 0;
`

Title.Letter = styled.span`
  display: inline-block;
  overflow: hidden;
`

Title.Container = styled.h1`
  white-space: pre-wrap;

  &.animate ${Title.Text} {
    animation: ${show} 0.5s var(--delay) ease both;
  }
`

export default Title
