import styled, { css } from 'styled-components'

export default styled.div`
  ${props => css`
    display: ${props.display};
    grid-template-columns: ${props.gridTemplateColumns};
    grid-gap: ${props.gridGap};
    align-items: ${props.alignItems};
    max-width: ${props.maxWidth || '36rem'};
    margin: 0 auto;

    @media screen and (max-width: ${props.media}) {
      grid-template-columns: 1fr;
    }
  `};
`
