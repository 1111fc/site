import styled from 'styled-components'

const Intro = {}

Intro.Wrapper = styled.div`
  position: fixed;
  z-index: 99;
  top: 0;
  left: 0;
  padding: 4rem 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.75);
`

Intro.Close = styled.button`
  position: absolute;
  top: 1rem;
  right: 1rem;
  z-index: 1;
  cursor: pointer;
  background-color: transparent;
  border: none;
  color: white;
  font-size: 2em;
`

Intro.Backdrop = styled.div`
  position: fixed;
  z-index: 0;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  background-color: var(--black);
`

Intro.Video = styled.iframe`
  position: relative;
  width: 100%;
  height: 100%;
`

Intro.Image = styled.div`
  position: absolute;
  z-index: 0;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  opacity: 1;

  img {
    position: absolute;
    z-index: 0;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: 50% 0%;
  }

  &::after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.65);
  }
`

Intro.Title = styled.div`
  position: absolute;
  z-index: 2;
  bottom: 0;
  left: 0;
  padding: var(--padding);
  width: 100%;
  text-align: center;
  font-size: 1.5rem;
  letter-spacing: 0.1em;
`

export default Intro
