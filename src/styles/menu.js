import styled, { css, keyframes } from 'styled-components'

const show = keyframes`
  from {
    opacity: 0;
    transform: translate(1rem, 0);
  }
  to {
    opacity: 1;
    transform: translate(0, 0);
  }
`

const hide = keyframes`
  from {
    opacity: 1;
    transform: translate(0, 0);
  }
  to {
    opacity: 0;
    transform: translate(1rem, 0);
  }
`

const Menu = {}

Menu.Container = styled.ul`
  ${({ isMobile, active }) => css`
    font-size: 0.85rem;

    ${isMobile
      ? css`
          position: absolute;
          top: 100%;
          right: 0;
          margin-top: 3rem;
          width: 10rem;
          pointer-events: none;
        `
      : css`
          display: flex;
        `};

    ${active &&
      css`
        pointer-events: auto;
      `};
  `};
`

const getAnimation = status => {
  switch (status) {
    case 'active':
      return css`
        animation: ${show} 0.25s ease both;
      `
    case 'inactive':
      return css`
        animation: ${hide} 0.25s ease both;
      `
    default:
      return ''
  }
}

Menu.Item = styled.li`
  ${({ isMobile, status }) => css`
    opacity: 0;
    transform: translate(0, 1rem);

    ${isMobile
      ? css`
          & + & {
            margin-top: 0.75rem;
          }

          &:hover a {
            transform: translate(-0.5rem, 0);
          }
        `
      : css`
          & + & {
            margin-left: 1rem;
          }
        `};

    a {
      display: inline-block;
      font-weight: bold;
      transition: transform 0.125s ease;
    }

    ${getAnimation(status)};
  `};
`

export default Menu
