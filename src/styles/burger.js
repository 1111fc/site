import styled, { css } from 'styled-components'

const Burger = {}

Burger.Container = styled.button`
  display: flex;
  align-items: center;
  font-size: 0.65em;
`

Burger.Label = styled.span`
  ${({ active }) => css`
    transition: opacity 0.125s 0.35s ease;

    ${active &&
      css`
        opacity: 0;
        transition-delay: 0s;
      `};
  `};
`

Burger.Icon = styled.span`
  ${({ active }) => css`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-left: 1rem;
    width: 2rem;
    height: 1.35rem;

    span {
      width: 100%;
      border-bottom: 0.125rem solid var(--white);
      transition: transform 0.125s ease;

      ${active &&
        css`
          &:nth-child(1) {
            transform-origin: 50% 50%;
            transform: translate(0, 0.625rem) rotate(-45deg);
          }

          &:nth-child(2) {
            transform-origin: 50% 50%;
            transform: scale(0);
          }

          &:nth-child(3) {
            transform-origin: 50% 50%;
            transform: translate(0, -0.625rem) rotate(45deg);
          }
        `};
    }
  `};
`

export default Burger
