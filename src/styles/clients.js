import styled from 'styled-components'

const Client = {}

Client.Wrapper = styled.div`
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fill, minmax(10rem, 1fr));
  max-width: 48em;
  width: 100%;
`

Client.Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`

Client.Image = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1rem;
  width: 100%;

  &::before {
    content: '';
    display: block;
    padding-bottom: 100%;
  }

  & img {
    position: absolute;
    top: 0;
    left: 0;
    display: block;
    margin: 1rem;
    width: calc(100% - 2rem);
    height: calc(100% - 2rem);
    object-fit: contain;
  }
`

Client.Title = styled.div`
  text-align: center;
`

export default Client
