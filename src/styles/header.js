import styled, { css } from 'styled-components'

export const Nav = styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;

  & > * {
    pointer-events: auto;
  }

  ${({ isMobile }) =>
    isMobile &&
    `
    align-items: flex-start;
  `};
`

export const Logo = styled.div`
  min-width: 5rem;
  width: 6rem;
  animation: fadeIn 0.5s ease both;

  a {
    display: block;
  }

  svg .st-text {
    fill: var(--white);
  }

  svg .st-dots {
    fill: var(--orange);
  }
`

export const Menu = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  margin-left: 2rem;
  letter-spacing: 0.15em;
  animation: fadeIn 0.5s ease both;
`

export const Container = styled.header`
  ${({ isMobile, overlay }) => css`
    position: fixed;
    z-index: 5;
    top: 0;
    left: 0;
    width: 100%;
    height: ${isMobile ? '100vh' : 'auto'};
    padding: 1rem var(--padding);
    pointer-events: none;
    background-color: ${isMobile ? 'transparent' : 'rgba(0, 0, 0, 0.5)'};
    transition: background-color 0.25s 0.25s ease;

    ${overlay &&
      css`
        background-color: var(--overlay-color);
        transition: background-color 0.25s ease;
      `};

    @media screen and (max-width: 425px) {
      transition: none !important;
    }

    ${!isMobile &&
      `
      ${Menu} {
        font-size: 0.5rem;
      }
    `};
  `};
`
