---
key: works
order: 0
thumbnail: /assets/fecomercio-ep-04.jpg
videoId: 5lWaFRNpHMo
title: Fecomércio ep. 04 - Meu Pai
client: O Povo
---
Roteiro e Direção: Tadeu Marinho

Edição: Amanda Girão

Cenário 3D: Herman Dantas

Locução: Carri Costa
