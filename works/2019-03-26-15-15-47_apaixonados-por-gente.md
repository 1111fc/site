---
key: works
order: 0
thumbnail: /assets/fecomercio-ep-02.jpg
videoId: ZsdyI-cOESk
title: Fecomércio ep. 02 - Meninos Lindos
client: O Povo
---
Roteiro e Direção: Tadeu Marinho

Edição: João André Rezende

Modelagem e Animação: Pablo Ferrari e Fábio Bola

Locução: Haroldo Guimarães
