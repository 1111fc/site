---
key: works
order: 0
thumbnail: /assets/fecomercio-ep-03.jpg
videoId: J1hc8OnS8EA
title: Fecomércio ep. 03 - Zé Maria
client: O Povo
---
Roteiro e Direção: Tadeu Marinho

Edição: João André Rezende

Modelagem e Animação: Pablo Ferrari e Fábio Bola

Locução: Haroldo Guimarães
