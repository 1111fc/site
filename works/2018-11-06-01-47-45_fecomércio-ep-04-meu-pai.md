---
key: works
order: 0
thumbnail: /assets/fecomercio-ep-05.jpg
videoId: GyMRO2ydM4k
title: Fecomércio ep. 05 - O Comércio
client: O Povo
---
Roteiro e Direção: Tadeu Marinho

CLIENTE: O Povo

Edição: Amanda Girão

Cenário 3D: Herman Dantas

Locução: Carri Costa
